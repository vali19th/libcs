#ifndef _DArray_h
#define _DArray_h

#include "stdlib.h"
#include "assert.h"
#include "dbg.h"

#define DEFAULT_EXPAND_RATE 300

typedef struct DArray{
    int length;
    int max;
    size_t element_size;
    size_t expand_rate;
    void **contents;
} DArray;

DArray *DArray_create(size_t element_size, size_t initial_max);
void DArray_destroy(DArray *array);
void DArray_clear(DArray *array);

int DArray_expand(DArray *array);
int DArray_contract(DArray *array);

int DArray_push(DArray *array, void *element);
void *DArray_pop(DArray *array);


static inline void DArray_set(DArray *array, int i, void *elem) {
    check(i < array->max,
        "darray attempt to set past max.");
    if(i > array->length) array->length = i;
    array->contents[i] = elem;

    error:
    return;
}

static inline void *DArray_get(DArray *array, int i) {
    check(i < array->max,
        "darray attempt to get past max.");
    return array->contents[i];

    error:
    return NULL;
}


static inline void *DArray_add(DArray *array) {
    check(array->element_size > 0,
        "Can't use DArray_add on 0 size darrays.");
    return calloc(1, array->element_size);

    error:
    return NULL;
}

static inline void *DArray_del(DArray *array, int i) {
    void *elem = array->contents[i];
    array->contents[i] = NULL;
    return elem;
}

#endif
