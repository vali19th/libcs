#include "minunit.h"
#include "darray.h"

char *test_create_destory() {
    DArray *array = NULL;
    array = DArray_create(sizeof(int), 100);
    mu_assert(array != NULL,
        "DArray_create failed.");
    mu_assert(array->contents != NULL,
        "Wrong contents in darray.");
    mu_assert(array->length == 0,
        "Wrong darray length.");
    mu_assert(array->element_size == sizeof(int),
        "Wrong element_size.");
    mu_assert(array->max == 100,
        "Wrong darray max length after create.");

    DArray_destroy(array);
    return NULL;
}

char *test_add_del() {
    DArray *array = DArray_create(sizeof(int), 100);

    int *val_1 = DArray_add(array);
    int *val_2 = DArray_add(array);
    mu_assert(val_1 != NULL,
        "failed to add a new element.");
    mu_assert(val_2 != NULL,
        "failed to add a new element.");

    DArray_set(array, 0, val_1);
    DArray_set(array, 1, val_2);

    int *val_check = DArray_del(array, 0);
    mu_assert(val_check != NULL,
        "Should not get NULL.");
    mu_assert(*val_check == *val_1,
        "Should get the first value.");
    mu_assert(DArray_get(array, 0) == NULL,
        "The first value should be gone.");
    free(val_check);

    val_check = DArray_del(array, 1);
    mu_assert(val_check != NULL,
        "Should not get NULL.");
    mu_assert(*val_check == *val_2,
        "Should get the second value.");
    mu_assert(DArray_get(array, 1) == NULL,
        "The second value should be gone.");
    free(val_check);

    DArray_destroy(array);
    return NULL;
}

char *test_set_get() {
    DArray *array = DArray_create(sizeof(int), 100);

    int *val_1 = DArray_add(array);
    DArray_set(array, 0, val_1);
    mu_assert(DArray_get(array, 0) == val_1,
        "Wrong first value.");

    int *val_2 = DArray_add(array);
    DArray_set(array, 1, val_2);
    mu_assert(DArray_get(array, 1) == val_2,
        "Wrong second value.");

    DArray_destroy(array);
    return NULL;
}

char *test_expand_contract() {
    DArray *array = DArray_create(sizeof(int), 100);

    int old_max = array->max;
    DArray_expand(array);
    mu_assert((unsigned int)array->max == old_max + array->expand_rate,
        "Wrong max length after expand.");

    DArray_contract(array);
    mu_assert((unsigned int)array->max == array->expand_rate + 1,
        "Should stay at the expand_rate at least.");

    DArray_contract(array);
    mu_assert((unsigned int)array->max == array->expand_rate + 1,
        "Should stay at the expand_rate at least.");

    DArray_destroy(array);
    return NULL;
}

char *test_push_pop() {
    DArray *array = DArray_create(sizeof(int), 100);

    for (int i = 0; i < 1000; i++) {
        int *val = DArray_add(array);
        *val = i * 333;
        DArray_push(array, val);
    }

    mu_assert(array->max = 1201,
        "Wrong max length.")

    for (int i = 999; i >= 0; i--) {
        int *val = DArray_pop(array);
        mu_assert(val != NULL,
            "Shouldn't get a NULL.");
        mu_assert(*val == i * 333,
            "Wrong value.");
        free(val);
    }

    DArray_destroy(array);
    return NULL;
}

char *all_tests() {
    mu_suite_start();

    mu_run_test(test_create_destory);
    mu_run_test(test_add_del);
    mu_run_test(test_set_get);
    mu_run_test(test_expand_contract);
    mu_run_test(test_push_pop);

    return NULL;
}

RUN_TESTS(all_tests);
