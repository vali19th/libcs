#include "list.h"
#include "dbg.h"

void Node_swap(Node *node_1, Node *node_2) {
    void *temp = node_1->value;
    node_1->value = node_2->value;
    node_2->value = temp;
}

List *List_create() {
    List *list = calloc(1, sizeof(List));
    check_mem(list);

    return list;

    error:
    return NULL;
}

void List_clear(List *list) {
    check(list != NULL, "Got NULL list as input.");
    LIST_FOREACH(list, node) {
        if (node->value) {
            node->value = NULL;
        }
    }
    check((list->length > 0 && (list->first != NULL || list->last != NULL)) || list->length == 0, "Wrong length after clear.");

    error:
    return;
}

void List_destroy(List *list) {
    check(list != NULL, "Got NULL list as input.");
    LIST_FOREACH(list, node) {
        if (node->prev) {
            List_dequeue(list);
        }
    }
    free(list->first);
    free(list);

    error:
    return;
}


void List_push(List *list, char *value) {
    check(list != NULL, "Got NULL list as input.");
    Node *node = calloc(1, sizeof(Node));
    check_mem(node);

    node->value = value;

    if (list->last == NULL) {
        list->first = node;
        list->last = node;
    } else {
        list->last->next = node;
        node->prev = list->last;
        list->last = node;
    }
    list->length++;
    check((list->length > 0 && (list->first != NULL || list->last != NULL)) || list->length == 0, "Wrong length after push.");

    error:
    return;
}

void List_queue(List *list, char *value) {
    check(list != NULL, "Got NULL list as input.");
    Node *node = calloc(1, sizeof(Node));
    check_mem(node);

    node->value = value;

    if (list->first == NULL) {
        list->first = node;
        list->last = node;
    } else {
        node->next = list->first;
        list->first->prev = node;
        list->first = node;
    }
    list->length++;
    check((list->length > 0 && (list->first != NULL || list->last != NULL)) || list->length == 0, "Wrong length after queue.");

    error:
    return;
}


char *List_pop(List *list) {
    check(list != NULL, "Got NULL list as input.");
    Node *node = list->last;
    check((list->length > 0 && (list->first != NULL || list->last != NULL)) || list->length == 0, "Wrong length after pop.");

    return node != NULL ? List_remove(list, node) : NULL;

    error:
    return NULL;
}

char *List_dequeue(List *list) {
    check(list != NULL, "Got NULL list as input.");
    Node *node = list->first;
    check((list->length > 0 && (list->first != NULL || list->last != NULL)) || list->length == 0, "Wrong length after dequeue.");

    return node != NULL ? List_remove(list, node) : NULL;

    error:
    return NULL;
}

char *List_remove(List *list, Node *node) {
    void *result = NULL;
    check(list != NULL, "Got NULL list as input.");
    check(list->first && list->last, "List is empty.");
    check(node, "Node can't be NULL.");

    if (node == list->first && node == list->last) {
        list->first = NULL;
        list->last = NULL;
    } else if (node == list->first) {
        list->first = node->next;
        check(list->first != NULL, "Invalid list, the first element is NULL.");
        list->first->prev = NULL;
    } else if (node == list->last) {
        list->last = node->prev;
        check(list->last != NULL, "Invalid list, got a next that is NULL.");
        list->last->next = NULL;
    } else {
        Node *after = node->next;
        Node *before = node->prev;
        after->prev = before;
        before->next = after;
    }
    list->length--;
    result = node->value;
    free(node);
    check(list->length == 0 || (list->length > 0 && (list->first != NULL && list->last != NULL)), "Wrong length after remove.");

    error:
    return result;
}


List *List_copy(List *list) {
    check(list != NULL, "Got NULL list as input.");

    List *new_list = List_create();
    LIST_FOREACH(list, node) {
        List_push(new_list, node->value);
    }

    check(list->length == new_list->length && ((list->length > 0 && (list->first != NULL || list->last != NULL)) || list->length == 0), "Wrong length after copy.");

    return new_list;

    error:
    return NULL;
}

List *List_join(List *list_1, List *list_2) {
    check(list_1 != NULL && list_2 != NULL, "Got NULL list as input.");
    check(list_1->first && list_1->last, "List 1 is empty.");
    check(list_2->first && list_2->last, "List 2 is empty.");

    List *new_list = List_create();

    LIST_FOREACH(list_1, node_1) {
        List_push(new_list, node_1->value);
    }

    LIST_FOREACH(list_2, node_2) {
        List_push(new_list, node_2->value);
    }

    return new_list;

    error:
    return NULL;
}

List *List_merge_sort(List *list) {
    // lists of 0 or 1 elements are already sorted
    if (list->length < 2) return List_copy(list);

    // find the node in the middle of the list
    int middle = list->length / 2;
    Node *mid = list->first;
    for (int i = 0; i < middle; i++) {
        mid = mid->next;
    }

    List *left = List_create();
    List *right = List_create();
    List_split(list, mid, left, right);

    void *val = NULL;
    List *new_list = List_create();
    List *sort_left = List_merge_sort(left);
    List *sort_right = List_merge_sort(right);
    List_destroy(left);
    List_destroy(right);

    while(sort_left->length > 0 || sort_right->length > 0) {
        if (sort_left->length > 0 && sort_right->length > 0) {
            if (strcmp(sort_left->first->value, sort_right->first->value) <= 0) {
                val = List_dequeue(sort_left);
            } else {
                val = List_dequeue(sort_right);
            }
        } else if (sort_left->length > 0) {
            val = List_dequeue(sort_left);
        } else if (sort_right->length > 0) {
            val = List_dequeue(sort_right);
        }
        List_push(new_list, val);
    }

    List_destroy(sort_left);
    List_destroy(sort_right);

    return new_list;
}

List *List_bubble_sort(List *list) {
    List *new_list = List_copy(list);
    if (list->length < 2) return new_list;

    // calculate list length
    int n = 0;
    LIST_FOREACH(list, _) { n++; }

    Node *node;
    int new_n;

    do {
        new_n = 0;
        node = new_list->first;
        for (int i = 1; i < n; i++) {
            if (node->next) {
                if (strcmp(node->value, node->next->value) > 0) {
                    Node_swap(node, node->next);
                    new_n = i;
                }
            }
        }
        n = new_n;
    } while(n);

    return new_list;
}

void List_split(List *list, Node *split_at, List *list_1, List *list_2) {
    check(list != NULL, "Got NULL list as input.");
    check(list->first && list->last, "List is empty.");
    check(split_at != NULL, "Got NULL node as input.");

    int push_to_1st = 1;
    LIST_FOREACH(list, node) {
        if(push_to_1st){
            List_push(list_1, node->value);
            if(node->next == split_at) push_to_1st = 0;
        } else {
            List_push(list_2, node->value);
        }
    }

    error:
    return;
}
