#undef NDEBUG
#ifndef _minunit_h
#define _minunit_h

#include "stdio.h"
#include "dbg.h"
#include "stdlib.h"

#define mu_suite_start() char *message = NULL
#define mu_assert(test, message) if(!(test)) {log_err(message); return message;}
#define mu_run_test(test) debug("\n---%s", " " #test); message = test(); tests_run++; if(message) return message;

#define RUN_TESTS(name) int main(int argc, char *argv[]) {\
    argc = argc;\
    debug("\n----- RUNNING: \x1b[36m%s\x1B[0m\n", argv[0]);\
    printf("---\nRUNNING: \x1b[36m%s\x1B[0m\n", argv[0]);\
    char *result = name();\
    if (result != 0) {\
        printf("\x1b[31m[FAILED]\x1B[0m: %s\n", result);\
    } else {\
        printf("\x1b[32m[OK]\x1B[0m\n");\
    }\
    printf("Tests run: \x1b[36m%d\x1B[0m\n", tests_run);\
    exit(result != 0);\
}

int tests_run;

#endif
