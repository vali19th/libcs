echo $(tput setaf 6)Running unit tests:$(tput setaf 7)

for i in tests/*_tests; do
    if test -f $i; then
        if $VALGRIND ./$i 2>> tests/tests.log; then
            echo $i $(tput setaf 2)[OK]$(tput setaf 7)
        else
            echo $(tput setaf 1)[ERROR]$(tput setaf 7) in test $(tput setaf 6)$i$(tput setaf 7): here is tests/tests.log
            echo -----
            tail tests/tests.log
            exit 1
        fi
    fi
done

echo $(tput setaf 2)[DONE]$(tput setaf 7)
