#ifndef List_h
#define List_h

#include "stdlib.h"

struct Node;

typedef struct Node {
    struct Node *next;
    struct Node *prev;
    void *value;
} Node;

typedef struct List {
    int length;
    Node *first;
    Node *last;
} List;

void Node_swap(Node *node_1, Node *node_2);

List *List_create();
void List_clear(List *list);
void List_destroy(List *list);

void List_push(List *list, char *value);
void List_queue(List *list, char *value);

char *List_pop(List *list);
char *List_dequeue(List *list);
char *List_remove(List *list, Node *node);

List *List_copy(List *list);
List *List_join(List *list_1, List *list_2);
List *List_merge_sort(List *list);
List *List_bubble_sort(List *list);
void List_split(List *list, Node *split_at, List *list_1, List *list_2);


#define LIST_FOREACH(list, node) Node *node = NULL;\
    for(node = list->first; node != NULL; node = node->next)

#define LIST_REVERSE_FOREACH(list, node) Node *node = NULL;\
    for(node = list->last; node != NULL; node = node->prev)

#endif
