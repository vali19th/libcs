# libcs (Computer Science)

This library contains data structures and algorithms.

Tested with: gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.4)  
This library is based on code from "Learn C the hard way" by Zed A. Shaw  
Any tips and corrections are welcome.  

#### How to use it:  
- run 'make' in terminal -- it will compile the library and run the unit tests
