#include "minunit.h"
#include "list.h"

char *values[] = {"XXXXX", "1234", "abcd", "qwerty", "xjvef"};
#define NUM_VALUES 5

void push_values(List *list) {
    for (int i = 0; i < NUM_VALUES; i++) {
        List_push(list, values[i]);
    }
}

int is_sorted(List *list) {
    LIST_FOREACH(list, node) {
        if (node->next && strcmp(node->value, node->next->value) > 0) {
            debug("%s %s", (char *)node->value, (char *)node->next->value);
            return 0;
        }
    }

    return 1;
}



char *test_create() {
    List *list = NULL;
    list = List_create();

    mu_assert(list != NULL,
        "Failed to create list.");
    mu_assert(list->length == 0,
        "Wrong length after create.");
    mu_assert(list->first == NULL || list->last == NULL,
        "The list should be empty.");

    List_destroy(list);
    return NULL;
}

char *test_clear() {
    List *list = List_create();
    push_values(list);
    List_clear(list);

    LIST_FOREACH(list, node) {
        mu_assert(node->value == NULL,
            "Wrong value after clear.");
    }

    List_destroy(list);
    return NULL;
}

char *test_destroy() {
    List *list = List_create();
    push_values(list);
    List_destroy(list);

    return NULL;
}


char *test_push_pop() {
    List *list = List_create();
    char *val = NULL;

    List_push(list, values[0]);
    mu_assert(list->last->value == values[0],
        "Wrong value after push.");
    mu_assert(list->length == 1,
      "Wrong length after push.");

    List_push(list, values[1]);
    mu_assert(list->last->value == values[1],
        "Wrong value after push.");
    mu_assert(list->length == 2,
        "Wrong length after push.");

    List_push(list, values[2]);
    mu_assert(list->last->value == values[2],
        "Wrong value after push.");
    mu_assert(list->length == 3,
        "Wrong length after push.");


    val = List_pop(list);
    mu_assert(val == values[2],
        "Wrong value after pop.");
    mu_assert(list->length == 2,
        "Wrong length after pop.");

    val = List_pop(list);
    mu_assert(val == values[1],
        "Wrong value after pop.");
    mu_assert(list->length == 1,
        "Wrong length after pop.");

    val = List_pop(list);
    mu_assert(val == values[0],
        "Wrong value after pop.");
    mu_assert(list->length == 0,
        "Wrong length after pop.");

    List_destroy(list);
    return NULL;
}

char *test_queue_dequeue() {
    List *list = List_create();
    char *val = NULL;

    List_queue(list, values[0]);
    mu_assert(list->first->value == values[0],
        "Wrong value after queue.");
    mu_assert(list->length == 1,
        "Wrong length after queue")

    List_queue(list, values[1]);
    mu_assert(list->first->value == values[1],
        "Wrong value after queue.");
    mu_assert(list->length == 2,
        "Wrong length after queue")

    List_queue(list, values[2]);
    mu_assert(list->first->value == values[2],
        "Wrong value after queue.");
    mu_assert(list->length == 3,
        "Wrong length after queue")

    val = List_dequeue(list);
    mu_assert(val == values[2],
        "Wrong value after dequeue.");
    mu_assert(list->length == 2,
        "Wrong length after dequeue.");

    val = List_dequeue(list);
    mu_assert(val == values[1],
        "Wrong value after dequeue.");
    mu_assert(list->length == 1,
        "Wrong length after dequeue.");

    val = List_dequeue(list);
    mu_assert(val == values[0],
        "Wrong value after dequeue.");
    mu_assert(list->length == 0,
        "Wrong length after dequeue.");

    List_destroy(list);
    return NULL;
}

char *test_remove() {
    List *list = List_create();
    push_values(list);

    // we only need to test the middle remove case since pop/dequeue already test the other cases
    char *val = List_remove(list, list->first->next);
    mu_assert(val == values[1],
        "Wrong element removed.");
    mu_assert(list->length == 4,
        "Wrong length after remove.");
    mu_assert(list->first->value == values[0],
        "Wrong first after remove.");
    mu_assert(list->last->value == values[4],
        "Wrong last after remove.");

    List_destroy(list);
    return NULL;
}


char *test_copy() {
    List *list_1 = List_create();
    push_values(list_1);

    List *list_2 = List_copy(list_1);

    Node *node_2 = list_2->first;
    LIST_FOREACH(list_1, node) {
        mu_assert(strcmp(node->value, node_2->value) == 0,
            "Wrong value after copy");
        node_2 = node_2->next;
    }

    List_destroy(list_1);
    List_destroy(list_2);
    return NULL;
}

char *test_join() {
    List *list_1 = List_create();
    List *list_2 = List_create();
    push_values(list_1);
    push_values(list_2);

    List *new_list = List_join(list_1, list_2);
    mu_assert(new_list->length == list_1->length + list_2->length,
        "Wrong length after join.");
    mu_assert(new_list->first->value == list_1->first->value,
        "Wrong first after join.");
    mu_assert(new_list->last->value == list_2->last->value,
        "Wrong last after join.");

    Node *node_1 = list_1->first;
    Node *node_2 = list_2->first;
    LIST_FOREACH(new_list, node) {
        if (node->value == node_1->value){
            mu_assert(strcmp(node->value, node_1->value) == 0,
                "Wrong value after join.");
            node_1 = node_1->next;
        } else if (node->value == node_2->value) {
            mu_assert(strcmp(node->value, node_2->value) == 0,
                "Wrong value after join.");
            node_2 = node_2->next;
        }
    }

    List_destroy(list_1);
    List_destroy(list_2);
    List_destroy(new_list);
    return NULL;
}

char *test_merge_sort() {
    // test on empty list
    List *input = List_create();
    List *empty = List_merge_sort(input);
    mu_assert(is_sorted(empty),
        "Merge sort failed on empty list.");

    // test on random list
    push_values(input);
    List *random = List_merge_sort(input);
    mu_assert(is_sorted(random),
        "Merge sort failed on random list.");

    // test on sorted list
    List *sorted = List_merge_sort(random);
    mu_assert(is_sorted(sorted),
        "Merge sort failed on sorted list.");

    List_destroy(input);
    List_destroy(empty);
    List_destroy(random);
    List_destroy(sorted);

    return NULL;
}

char *test_bubble_sort() {
    // test on empty list
    List *input = List_create();
    List *empty = List_bubble_sort(input);
    mu_assert(is_sorted(empty),
        "Bubble sort failed on empty list.");

    // test on random list
    push_values(input);
    List *random = List_bubble_sort(input);
    mu_assert(is_sorted(random),
        "Bubble sort failed on random list.");

    // test on sorted list
    List *sorted = List_bubble_sort(random);
    mu_assert(is_sorted(sorted),
        "Bubble sort failed on sorted list.");

    List_destroy(input);
    List_destroy(empty);
    List_destroy(random);
    List_destroy(sorted);

    return NULL;
}

char *test_split() {
    List *list = List_create();
    push_values(list);

    List *list_1 = List_create();
    List *list_2 = List_create();
    List_split(list, list->first->next->next, list_1, list_2);

    mu_assert(list_1->length == 2 && list_2->length == 3,
        "Wrong length after split.");
    mu_assert(list_1->first->value == list->first->value,
        "Wrong first after split.");
    mu_assert(list_2->last->value == list->last->value,
        "Wrong last after split.");

    Node *node_1 = list_1->first;
    Node *node_2 = list_2->first;
    LIST_FOREACH(list, node) {
        if (node->value == node_1->value){
            mu_assert(strcmp(node->value, node_1->value) == 0,
                "Wrong value after split.");
            node_1 = node_1->next;
        } else if (node->value == node_2->value) {
            mu_assert(strcmp(node->value, node_2->value) == 0,
                "Wrong value after split.");
            node_2 = node_2->next;
        }
    }

    List_destroy(list_1);
    List_destroy(list_2);
    List_destroy(list);
    return NULL;
}



char *all_tests() {
    mu_suite_start();

    mu_run_test(test_create);
    mu_run_test(test_clear);
    mu_run_test(test_destroy);

    mu_run_test(test_push_pop);
    mu_run_test(test_queue_dequeue);
    mu_run_test(test_remove);

    mu_run_test(test_copy);
    mu_run_test(test_join);
    mu_run_test(test_merge_sort);
    mu_run_test(test_bubble_sort);
    mu_run_test(test_split);

    return NULL;
}

RUN_TESTS(all_tests);
